#include "specparser.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int argcmp(char **pos, char *arg_type) {
  int ret = strncasecmp(*pos, arg_type, strlen(arg_type));
  if (ret == 0) {
    *pos += strlen(arg_type);
  }
  return ret == 0;
}

ArgumentType argget(char **pos) {
  if (**pos == '\0')
    return INVALID;
  if (argcmp(pos, "ABS")) {
    return ABS;
  } else if (argcmp(pos, "IMM")) {
    return IMM;
  } else if (argcmp(pos, "ZPG")) {
    return ZPG;
  } else if (argcmp(pos, "IND")) {
    return IND;
  } else if (argcmp(pos, "PAG")) {
    return PAG;
  } else if (argcmp(pos, "BAS")) {
    return BAS;
  } else if (argcmp(pos, "LBL_ABS")) {
    return LBL_ABS;
  } else if (argcmp(pos, "LBL_REL")) {
    return LBL_REL;
  } else if (argcmp(pos, "FL")) {
    return FL;
  } else if (argcmp(pos, "SP")) {
    return SP;
  } else if (argcmp(pos, "PC")) {
    return PC;
  } else if (argcmp(pos, "A")) {
    return A;
  } else if (argcmp(pos, "B")) {
    return B;
  } else if (argcmp(pos, "C")) {
    return C;
  } else if (argcmp(pos, "D")) {
    return D;
  } else if (argcmp(pos, "E")) {
    return E;
  } else if (argcmp(pos, "F")) {
    return F;
  } else if (argcmp(pos, "P")) {
    return P;
  } else if (argcmp(pos, "Z")) {
    return Z;
  } else {
    return INVALID;
  }
}

Variant *varadd(Operation *op) {
  Variant *ptr;
  if (op->n_variants == 0) {
    op->variants = malloc(sizeof(Variant));
    ptr = op->variants;
    op->n_variants++;
  } else {
    op->n_variants++;
    op->variants = (Variant *)realloc(op->variants,
                                      ((op->n_variants) * (sizeof(Variant))));
    ptr = op->variants + (op->n_variants - 1);
  }
  ptr->opcode = 0;
  ptr->args = NULL;
  ptr->n_args = 0;
  return ptr;
}

ArgumentType *add_arg(Variant *variant) {
  ArgumentType *ptr;
  if (variant->n_args == 0) {
    variant->args = malloc(sizeof(ArgumentType));
    ptr = variant->args;
    variant->n_args++;
  } else {
    variant->n_args++;
    variant->args = (ArgumentType *)realloc(
        variant->args, ((variant->n_args) * (sizeof(ArgumentType))));
    ptr = variant->args + (variant->n_args - 1);
  }
  *ptr = INVALID;
  return ptr;
}

Operation *opadd(OpTable *table) {
  Operation *op = malloc(sizeof(Operation));
  strncpy(op->mnemonic, "ILLEGAL", 7);
  op->variants = NULL;
  op->n_variants = 0;
  op->next = NULL;
  op->prev = NULL;

  op->prev = table->tail;

  if (table->head == NULL) {
    table->head = op;
    table->tail = op;
  } else {
    table->tail->next = op;
    table->tail = op;
  }
  return op;
}

void cleanup(FILE *fp, OpTable *opt) {
  fclose(fp);
  optdestroy(opt);
}

OpTable *optinit() {
  OpTable *table = malloc(sizeof(OpTable));
  table->head = NULL;
  table->tail = NULL;

  FILE *spec = fopen(SPEC, "rw");
  char line[BUFFER_SIZE];
  char *pos;
  ArgumentType arg = INVALID;
  int opcode;

  // Current operation
  Operation *op = NULL;
  Variant *var = NULL;
  ArgumentType *parg = NULL;
  int ln = 0;
  int nlcount = 0;

  while (fgets(line, BUFFER_SIZE, spec) != NULL) {
    ln++;
    pos = (char *)line;
    if (isempty(pos)) {
      nlcount++;
      if (nlcount >= 2) {
        nlcount = 0;
        op = NULL;
        continue;
      }
      continue;
    }
    if (op == NULL || isalpha(*pos)) {
      // Skip as a comment
      if (!isalpha(*pos)) {
        continue;
      }
      op = opadd(table);
      sscanf(pos, "%10s", op->mnemonic);
      pos += strlen(op->mnemonic);
      skipws(&pos);
      continue;
    } else if (isspace(*pos)) {
      skipws(&pos);
      if (*pos == '0' &&
          (*(pos + 1) != '\0' && (*(pos + 1) == 'x' || *(pos + 1) == 'X'))) {
        if (sscanf(pos, "%x", &opcode) == 0) {
          cleanup(spec, table);
          die("err: parsing failed at: %d:%d. expected a hex notation\n", ln,
              (int)(pos - line));
        }
        pos += 2;

        while (isdigit(*pos) || (*pos >= 'a' && *pos <= 'f') ||
               (*pos >= 'A' && *pos <= 'F')) {
          pos++;
        }
      } else {
        if (sscanf(pos, "%d", &opcode) == 0) {
          cleanup(spec, table);
          die("err: parsing failed at: %d:%d. expected a decimal notation\n",
              ln, (int)(pos - line));
        }
        while (isdigit(*pos))
          pos++;
      }

      var = varadd(op);
      memcpy((void *)(&(var->opcode)), (void *)&opcode, sizeof(opcode));
      skipws(&pos);
      while (*pos != '\n' && *pos != '\0') {
        skipws(&pos);
        arg = argget(&pos);
        if (arg == INVALID) {
          cleanup(spec, table);
          die("err: parsing failed at: %d:%d. unknown argument type\n", ln,
              (int)(pos - line));
        }
        skipws(&pos);
        parg = add_arg(var);
        *parg = arg;
        if (*pos != ',') {
          if (*pos == '\n' || *pos == '\0')
            continue;
          cleanup(spec, table);
          die("err: parsing failed at: %d:%d. expected a comma or an EOL\n", ln,
              (int)(pos - line));
        } else {
          pos++;
          skipws(&pos);
        }
      }
      continue;
    }
  }
  fclose(spec);
  return table;
}

void optdestroy(OpTable *table) {
  Operation *op = table->tail;
  Operation *prev = NULL;
  int n_variant;

  for (; op != NULL; op = table->tail) {
    // delete all the variants;
    for (n_variant = 0; n_variant < op->n_variants; n_variant++) {
      free(op->variants[n_variant].args);
    }
    free(op->variants);
    // keep a reference to the previous node
    prev = op->prev;
    // free the operation;
    free(op);
    table->tail = prev;
  }
  free(table);
}

void optprint(OpTable *table) {
  Operation *op = table->head;
  int vc = 0;
  int ac = 0;

  for (; op != NULL; op = op->next) {
    printf("mnemonic: '%s'\n", op->mnemonic);
    printf("\tn_variants: %d\n", op->n_variants);
    printf("--------------------------------------------------\n");
    for (vc = 0; vc < op->n_variants; vc++) {
      printf("\topcode: 0x%x\n", op->variants[vc].opcode);
      printf("\t\tn_args: %d", op->variants[vc].n_args);
      printf("\t\targs: ");
      for (ac = 0; ac < op->variants[vc].n_args; ac++) {
        printf("%d%s", op->variants[vc].args[ac],
               ac == op->variants[vc].n_args - 1 ? "" : ",");
      }
      printf("\n");
    }
  }
}

Operation *opfind(OpTable *table, char *instruction) {
  Operation *op = table->head;
  while (op != NULL) {
    if (strncmp(op->mnemonic, instruction, strlen(op->mnemonic)) == 0) {
      return op;
    }
    op = op->next;
  }
  return NULL;
}
