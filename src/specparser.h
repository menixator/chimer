#include "config.h"
#include "utils.h"
#include <stdio.h>

#ifndef SPECPARSER_H
#define SPECPARSER_H

enum ARGUMENT_TYPE {
  INVALID,
  ABS,
  IMM,
  ZPG,
  IND,
  PAG,
  BAS,
  LBL_ABS,
  LBL_REL,
  A,
  B,
  C,
  D,
  E,
  F,
  P,
  Z,
  FL,
  SP,
  PC
};

typedef enum ARGUMENT_TYPE ArgumentType;

typedef struct Variant {
  int opcode;
  ArgumentType *args;
  int n_args;
} Variant;

typedef struct Operation {
  char mnemonic[MNEMONIC_MAX_LENGTH];
  Variant *variants;
  int n_variants;
  struct Operation *next;
  struct Operation *prev;
} Operation;

typedef struct OpTable {
  Operation *head;
  Operation *tail;
} OpTable;

void cleanup(FILE *fp, OpTable *opt);
ArgumentType argget(char **pos);

// functions that deal with the Op Table
OpTable *optinit();
void optdestroy(OpTable *table);
void optprint(OpTable *table);
Operation *opfind(OpTable *table, char *instruction);
// functions that modify the Op Table
Variant *varadd(Operation *op);
Operation *opadd(OpTable *table);
int argcmp(char **pos, char *arg_type);

#endif
