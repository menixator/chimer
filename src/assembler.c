#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"
#include "jumptable.h"
#include "specparser.h"

typedef struct Argument {
  ArgumentType type;
  unsigned char data[DATA_FIELD_SIZE];
} Argument;

typedef struct Instruction {
  char mnemonic[MNEMONIC_MAX_LENGTH];
  Argument args[MAX_ARGS];
} Command;

int main(int argc, char **argv) {
  if (argc <= 1) {
    printf("Not enough arguments\n");
    return 1;
  }

  char *input = argv[1];
  size_t length;
  char line[BUFFER_SIZE];
  char *seek;
  FILE *fp = fopen(input, "r");
  if (fp == NULL) {
    die("err: failed to open the file '%s'\n", input);
  }
  OpTable *table = optinit();
  JumpTable *jmptbl = jmptblinit();
  while (fgets(line, BUFFER_SIZE, fp) != NULL) {
    length = strlen(line);
    if (length == 0)
      continue;
    seek = (char *)line;

    skipws(&seek);

    if (*seek == '\0' || !isprint(*seek)) {

      continue;
    }
  }
  cleanup(fp, table);
  jmptbldestroy(jmptbl);
  return 0;
}
