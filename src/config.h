
#ifndef CONFIG_H
#define CONFIG_H

#define MAX_ARGS 2
#define MAX_VARIANTS 4
#define MNEMONIC_MAX_LENGTH 10
#define SPEC "./spec"
#define BUFFER_SIZE 256
#define DATA_FIELD_SIZE 2
#define LABEL_MAX_LENGTH 32
typedef short int ADDRESS;
#define ADDRESS_SIZE sizeof(ADDRESS)
#endif
