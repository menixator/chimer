#include "utils.h"

void skipws(char **ptr) {
  while (isspace(**ptr) && **ptr != '\0' && **ptr != '\n') {
    (*ptr)++;
  }
}

bool isempty(char *ptr) {
  while (*ptr != '\0') {
    if (!isspace(*ptr))
      return false;
    ptr++;
  }
  return true;
}

void die(const char *format, ...) {
  va_list args;
  va_start(args, format);
  vfprintf(stderr, format, args);
  va_end(args);
  exit(1);
}
