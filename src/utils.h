#include <ctype.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef UTILS_H
#define UTILS_H

// Helpers
void skipws(char **ptr);
bool isempty(char *ptr);
void die(const char *format, ...);

#endif
