#include "jumptable.h"

JumpTable *jmptblinit() {
  JumpTable *jmptbl = malloc(sizeof(JumpTable));
  jmptbl->head = NULL;
  jmptbl->tail = NULL;
  jmptbl->n_labels = 0;
  return jmptbl;
}

Label *labelfind(JumpTable *jmptbl, char *name) {
  Label *label = jmptbl->head;
  while (label != NULL) {
    if (strcmp(label->name, name) == 0) {
      return label;
    }
    label = label->next;
  }
  return NULL;
}

void addreference(Label *label, long seekpos) {
  if (label->n_references == 0) {
    label->references = malloc(sizeof(long));
    *label->references = seekpos;
  } else {
    label->references =
        realloc(label->references, sizeof(label->n_references + 1));
    label->references[label->n_references] = seekpos;
  }
}

Label *labeladd(JumpTable *jmptbl, char *name) {
  Label *label = malloc(sizeof(Label));
  strncpy(label->name, name, LABEL_MAX_LENGTH);
  label->references = NULL;
  label->n_references = 0;
  jmptbl->tail->next = label;
  label->prev = jmptbl->tail;
  jmptbl->tail = label;
  label->next = NULL;
  return label;
}

void jmptbldestroy(JumpTable *jmptbl) {
  Label *label = jmptbl->tail;
  Label *prev = NULL;
  while (label != NULL) {
    prev = label->prev;
    free(label);
    label = prev;
  }
  free(jmptbl);
}
