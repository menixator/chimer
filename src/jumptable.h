
#ifndef JUMPTABLE_H
#define JUMPTABLE_H
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Label {
  char name[LABEL_MAX_LENGTH];
  // Seek positions where addresses are stored.
  long *references;
  ADDRESS address;
  int n_references;
  struct Label *next;
  struct Label *prev;
} Label;

typedef struct JumpTable {
  Label *head;
  Label *tail;
  int n_labels;
} JumpTable;
JumpTable *jmptblinit();
Label *labeladd(JumpTable *jmptbl, char *name);
Label *labelfind(JumpTable *jmptbl, char *name);
void jmptbldestroy(JumpTable *jmptbl);
void addreference(Label *label, long seekpos);
#endif
